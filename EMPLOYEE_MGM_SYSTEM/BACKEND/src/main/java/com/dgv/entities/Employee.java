package com.dgv.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "VAIBHAV_KULKARNI_SPRING_EMPLOYEES")
@Data
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "employee_id")
	private Long employeeId;

	@Column(name = "employee_name", nullable = false)
	private String employeeName;

	@Column(name = "email", unique = true, nullable = false, length = 255)
	private String email;

	@Column(name = "password", nullable = false)
	private String password;

	@Column(name = "performance_metrics")
	private Double performanceMetrics;

	@Enumerated(EnumType.STRING)
	@Column(name = "role", nullable = false)
	private Roles role;

	@Column(name = "mgr_id")
	private Long managerId;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Task> tasks;

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Double getPerformanceMetrics() {
		return performanceMetrics;
	}

	public void setPerformanceMetrics(Double performanceMetrics) {
		this.performanceMetrics = performanceMetrics;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", email=" + email
				+ ", password=" + password + ", performanceMetrics=" + performanceMetrics + ", role=" + role
				+ ", managerId=" + managerId + "]";
	}

	public Employee(Long employeeId2, Roles role2) {
		this.employeeId=employeeId2;
		this.role=role2;
	}

	public Employee() {
	}

	public Employee(Long employeeId2, String employeeName2, String email2, Roles role2) {
		this.employeeId=employeeId2;
		this.employeeName=employeeName2;
		this.email=email2;
		this.role=role2;
}

}
