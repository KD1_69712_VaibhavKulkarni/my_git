package com.dgv.entities;

public enum Rating {

	EASY(5), MEDIUM(10), HARD(15);

	private int rate;

	private Rating(int rate) {
		this.rate = rate;
	}

	public int getRate() {
		return rate;
	}

}
