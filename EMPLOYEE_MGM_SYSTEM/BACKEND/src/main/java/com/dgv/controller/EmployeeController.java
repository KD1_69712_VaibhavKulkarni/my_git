package com.dgv.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dgv.dto.AssignTaskDTO;
import com.dgv.entities.Task;
import com.dgv.service.IEmployeeService;
import com.dgv.service.IManagerService;

@RestController
@RequestMapping("/employee")
@CrossOrigin
public class EmployeeController {

	private static final Logger log = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	private IEmployeeService employeeService;

	@Autowired
	private IManagerService managerService;

	@PostMapping("/viewmetrics/{empId}")
	public ResponseEntity<?> viewAllMetricsOfEmployee(@PathVariable("empId") long empId) throws Exception {
	    log.info("Viewing metrics for employee with ID: {}", empId);
	    // Fetch metrics of the employee from the service
	    Double metrics = employeeService.getMetricsOfEmployee(empId);
	    log.info("Metrics fetched successfully for employee: {}", empId);
	    return new ResponseEntity<>(metrics, HttpStatus.OK);
	}


	@PostMapping("/incompletetasks/{empId}")
	public ResponseEntity<?> viewAllIncompleteTasks(@PathVariable("empId") long empId) throws Exception {
	    log.info("Viewing incomplete tasks for employee with ID: {}", empId);
	    try {
	        // Fetch all incomplete tasks of the employee from the service
	        List<Task> list = employeeService.getIncompleteTasksOfEmployees(empId);
	        List<AssignTaskDTO> listDto = new ArrayList<>();
	        for (Task t : list) {
	            // Convert tasks to DTOs and set additional information
	            AssignTaskDTO dtos = AssignTaskDTO.ToDto(t);
	            dtos.setDifficulty(t.getTaskDifficulty().toString());
	            listDto.add(dtos);
	        }
	        log.info("Incomplete tasks fetched successfully for employee: {}", empId);
	        return new ResponseEntity<>(listDto, HttpStatus.OK);
	    } catch (Exception e) {
	        log.error("Error occurred while fetching incomplete tasks for employee: {}", empId);
	        return new ResponseEntity<>("No employee found", HttpStatus.OK);
	    }
	}


	@PostMapping("/completethetask/{taskId}")
	public ResponseEntity<?> completeTheIncompleteTask(@PathVariable("taskId") long taskId) throws Exception {
	    log.info("Completing the incomplete task with ID: {}", taskId);
	    // Complete the task with the given ID using the service
	    String s = employeeService.completeTaskWithTaskId(taskId);
	    log.info("Task completed successfully with ID: {}", taskId);
	    return new ResponseEntity<>(s, HttpStatus.OK);
	}


	@PostMapping("/viewtask/{empId}")
	public ResponseEntity<?> viewAllTaskOfEmployee(@PathVariable("empId") long empId) throws Exception {
	    log.info("Viewing all tasks for employee with ID: {}", empId);
	    // Fetch all tasks of the employee from the manager service
	    List<Task> list = managerService.getAllTasks(empId);
	    List<AssignTaskDTO> listDto = new ArrayList<>();
	    for (Task t : list) {
	        // Convert tasks to DTOs and set additional information
	        AssignTaskDTO dtos = AssignTaskDTO.ToDto(t);
	        dtos.setDifficulty(t.getTaskDifficulty().toString());
	        listDto.add(dtos);
	    }
	    log.info("All tasks fetched successfully for employee: {}", empId);
	    return new ResponseEntity<>(listDto, HttpStatus.OK);
	}

}
