package com.dgv.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dgv.dto.EmailAndPasswordDto;
import com.dgv.dto.EmployeeDTO;
import com.dgv.dto.OldAndNewPasswordDto;
import com.dgv.entities.Employee;
import com.dgv.service.IUserService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@CrossOrigin
@Slf4j
public class UserController {

	@Autowired
	private IUserService userService;

	// API for user login to verify user credentials
	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@RequestBody EmailAndPasswordDto login) {
		log.info("Authenticating user: {}", login.getEmail());
		try {
			Employee user = userService.authenticateUser(login.getEmail(), login.getPassword());
			log.info("User authenticated successfully: {}", user.getEmployeeId());
			return new ResponseEntity<>(
					new Employee(user.getEmployeeId(), user.getEmployeeName(), user.getEmail(), user.getRole()),
					HttpStatus.OK);
		} catch (Exception e) {
			log.error("User authentication failed: {}", e.getMessage());
			return new ResponseEntity<>("No Credentials", HttpStatus.BAD_GATEWAY);
		}
	}

    // API to change password for an employee
    @PostMapping("/changepassword/{empid}")
    public ResponseEntity<?> changePassword(@PathVariable Long empid, @RequestBody OldAndNewPasswordDto dto) {
        log.info("Changing password for employee: {}", empid);
        log.debug("New password: {}", dto.getNewPassword());
        
        // Get the user by ID
        Employee user = userService.getUserById(empid);
        if (user != null) {
            // Check if the old password matches
            if (!user.getPassword().equals(dto.getOldPassword())) {
                log.error("Old password does not match for employee: {}", empid);
                return new ResponseEntity<>("Old password is incorrect.", HttpStatus.BAD_REQUEST);
            }
            
            // Check if the new password is the same as the old password
            if (user.getPassword().equals(dto.getNewPassword())) {
                log.error("New password cannot be the same as the old password for employee: {}", empid);
                return new ResponseEntity<>("New password should be different from the old password.", HttpStatus.BAD_REQUEST);
            }
            
            // Set the new password and update the user
            user.setPassword(dto.getNewPassword());
            userService.updateUser(user);
            
            log.info("Password changed successfully for employee: {}", empid);
            return new ResponseEntity<>("Password changed successfully.", HttpStatus.OK);
        } else {
            log.error("Employee not found: {}", empid);
            return new ResponseEntity<>("Employee not found.", HttpStatus.NOT_FOUND);
        }
    }


	// API to get the count of active users
	@GetMapping("/activeusers")
	public long getAllUsers() {
		log.info("Fetching count of active users");
		return userService.getAllUsers();
	}

}
