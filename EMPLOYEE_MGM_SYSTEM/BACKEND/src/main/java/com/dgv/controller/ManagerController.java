package com.dgv.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dgv.dto.AssignTaskDTO;
import com.dgv.dto.EmployeeDTO;
import com.dgv.dto.MetricsDto;
import com.dgv.entities.Employee;
import com.dgv.entities.Task;
import com.dgv.service.IManagerService;

@RestController
@RequestMapping("/manager")
@CrossOrigin
public class ManagerController {

	// Logger for logging messages
	private static final Logger log = LoggerFactory.getLogger(ManagerController.class);

	@Autowired
	private IManagerService managerService;

	// User register
	@PostMapping("/employeeregister")
	public ResponseEntity<?> addUser(@RequestBody EmployeeDTO employeedto) {
		log.info("Registering a new employee");

		Employee employee = EmployeeDTO.ToEntity(employeedto);
		try {
			employee = managerService.createNewUser(employee);
			log.info("Employee registered successfully");
			return new ResponseEntity<>(employee, HttpStatus.OK);

		} catch (Exception e) {
			log.error("Error occurred while registering employee");
			return new ResponseEntity<>("Email should be unique", HttpStatus.NOT_FOUND);

		}

	}

	// Task register
	@PostMapping("/taskregister")
	public ResponseEntity<?> addTask(@RequestBody AssignTaskDTO dto) {
		log.info("Registering a new task");

		Task task = AssignTaskDTO.ToEntity(dto);
		try {
			task = managerService.createNewTask(task, dto.getEmployeeId());
			AssignTaskDTO dtos = AssignTaskDTO.ToDto(task);
			dtos.setDifficulty(task.getTaskDifficulty().toString());
			log.info("Task registered successfully");
			return new ResponseEntity<>(dtos, HttpStatus.OK);

		} catch (Exception e) {
			log.error("Error occurred while registering task");
			return new ResponseEntity<>("Email should be unique", HttpStatus.NOT_FOUND);

		}

	}

	// View metrics
	@PostMapping("/metrics/{mgrid}")
	public ResponseEntity<?> viewMetrics(@PathVariable("mgrid") long mgrid) {
		log.info("Viewing metrics for manager with ID: {}", mgrid);

		List<MetricsDto> list = managerService.getMetricsOfAll(mgrid);
		log.info("Metrics fetched successfully for manager with ID: {}", mgrid);
		return new ResponseEntity<>(list, HttpStatus.OK);

	}

	// View all employees
	@PostMapping("/allemps/{mgrid}")
	public ResponseEntity<?> viewAllEmployees(@PathVariable("mgrid") long mgrid) {
		log.info("Viewing all employees for manager with ID: {}", mgrid);

		List<Employee> list = managerService.getAllEmployees(mgrid);
		log.info("All employees fetched successfully for manager with ID: {}", mgrid);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	// View all tasks of an employee
	@PostMapping("/viewtask/{empId}")
	public ResponseEntity<?> viewAllTaskOfEmployee(@PathVariable("empId") long empId) throws Exception {
		log.info("Viewing all tasks for employee with ID: {}", empId);

		List<Task> list = managerService.getAllTasks(empId);
		List<AssignTaskDTO> listDto = new ArrayList<>();
		for (Task t : list) {
			AssignTaskDTO dtos = AssignTaskDTO.ToDto(t);
			dtos.setDifficulty(t.getTaskDifficulty().toString());
			listDto.add(dtos);
		}
		log.info("All tasks fetched successfully for employee with ID: {}", empId);
		return new ResponseEntity<>(listDto, HttpStatus.OK);
	}
	
	// Delete task of an employee
	@PostMapping("/deletetask/{taskId}")
	public ResponseEntity<?> deleteTaskOfEmployee(@PathVariable("taskId") long taskId) throws Exception {
		log.info("Deleting task with ID: {}", taskId);

		String s = managerService.deleteTask(taskId);
		log.info("Task deleted successfully with ID: {}", taskId);
		return new ResponseEntity<>(s, HttpStatus.OK);
	}
	
	// Delete an employee
	@PostMapping("/{empId}")
	public ResponseEntity<?> deleteEmployee(@PathVariable("empId") long empId) throws Exception {
		log.info("Deleting employee with ID: {}", empId);

		String s = managerService.deleteEmployee(empId);
		log.info("Employee deleted successfully with ID: {}", empId);
		return new ResponseEntity<>(s, HttpStatus.OK);
	}
	
	// Edit an employee
	@PostMapping("/edit/{empId}")
	public ResponseEntity<?> editEmployee(@PathVariable("empId") long empId,@RequestBody Employee emp) throws Exception {
		log.info("Editing employee with ID: {}", empId);

		String s = managerService.editEmployee(empId,emp);
		log.info("Employee edited successfully with ID: {}", empId);
		return new ResponseEntity<>(s, HttpStatus.OK);
	}
	
	// Find an employee by ID
	@PostMapping("/findemployee/{empId}")
	public ResponseEntity<?> findEmployeeById(@PathVariable("empId") long empId) throws Exception {
		log.info("Finding employee with ID: {}", empId);

		Employee employee = managerService.findEmployeeById(empId);
		log.info("Employee found successfully with ID: {}", empId);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}
}
