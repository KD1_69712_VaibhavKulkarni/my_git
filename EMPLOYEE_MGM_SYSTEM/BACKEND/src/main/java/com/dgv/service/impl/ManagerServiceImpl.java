package com.dgv.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dgv.dto.MetricsDto;
import com.dgv.entities.Employee;
import com.dgv.entities.Roles;
import com.dgv.entities.Task;
import com.dgv.repository.IEmployeeRepo;
import com.dgv.repository.ITaskRepo;
import com.dgv.service.IManagerService;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class ManagerServiceImpl implements IManagerService {
	@Autowired
	private IEmployeeRepo employeeRepo;
	@Autowired
	private ITaskRepo taskRepo;

	public Employee createNewUser(Employee employee) {
		employee.setPerformanceMetrics(0.0);
		employee.setRole(Roles.EMPLOYEE);

		log.info("Creating a new user: {}", employee);
		return employeeRepo.save(employee);
	}

	@Override
	public Task createNewTask(Task task, Long id) throws Exception {
		task.setTaskStatus(false);
		Employee employee = employeeRepo.findById(id).orElseThrow(() -> new Exception("Employee not found"));
		task.setEmployee(employee);

		log.info("Creating a new task: {}", task);
		return taskRepo.save(task);
	}

	@Override
	public List<MetricsDto> getMetricsOfAll(long mgrid) {
		List<MetricsDto> dtos = new ArrayList<>();
		List<Employee> list = employeeRepo.findByManagerId(mgrid);
		List<Task> tasks = taskRepo.findAll();

		log.info("Getting metrics for manager with ID: {}", mgrid);

		for (Employee employee : list) {
			int sum = 0;
			for (Task t : tasks) {
				if (t.getEmployee() != null && employee.getEmployeeId() == t.getEmployee().getEmployeeId()
						&& (t.getTaskStatus())) {
					sum++;
				}
			}
			MetricsDto dto = new MetricsDto(employee.getEmployeeId(), employee.getEmployeeName(),
					employee.getPerformanceMetrics(), (long) sum);
			dtos.add(dto);
		}

		return dtos;
	}

	@Override
	public List<Employee> getAllEmployees(long mgrid) {
		log.info("Getting all employees for manager with ID: {}", mgrid);

		return employeeRepo.findByManagerId(mgrid);
	}

	@Override
	public List<Task> getAllTasks(long empId) throws Exception {
		Employee emp = employeeRepo.findById(empId).orElseThrow(() -> new Exception("Employee not found"));

		log.info("Getting all tasks for employee with ID: {}", empId);
		return taskRepo.findByEmployee(emp);
	}

	@Override
	public String deleteEmployee(long empId) {
		if (employeeRepo.existsById(empId)) {
			employeeRepo.deleteById(empId);
			log.info("Deleted employee with ID: {}", empId);
			return "Employee deleted successfully";
		} else {
			log.warn("Employee not found with ID: {}", empId);
			return "Employee not found";
		}
	}

	@Override
	public String editEmployee(long empId, Employee emp) {
		if (employeeRepo.existsById(empId)) {
			Employee employee = employeeRepo.findById(empId).get();
			employee.setEmail(emp.getEmail());
			employee.setEmployeeName(emp.getEmployeeName());
			employeeRepo.save(employee);

			log.info("Edited employee with ID: {}", empId);
			return "Employee edited successfully";
		} else {
			log.warn("Employee not found with ID: {}", empId);
			return "Employee not found";
		}
	}

	@Override
	public Employee findEmployeeById(long empId) {
		if (employeeRepo.existsById(empId)) {
			log.info("Finding employee with ID: {}", empId);
			return employeeRepo.findById(empId).get();
		} else {
			log.warn("Employee not found with ID: {}", empId);
			return null;
		}
	}

	@Override
	public String deleteTask(long taskId) {
		if (taskRepo.existsById(taskId)) {
			taskRepo.deleteById(taskId);
			log.info("Deleted task with ID: {}", taskId);
			return "Task deleted successfully";
		} else {
			log.warn("Task not found with ID: {}", taskId);
			return "Task not found";
		}
	}
}
