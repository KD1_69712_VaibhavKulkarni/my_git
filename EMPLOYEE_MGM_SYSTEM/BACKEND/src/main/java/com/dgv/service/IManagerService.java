package com.dgv.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.dgv.dto.MetricsDto;
import com.dgv.entities.Employee;
import com.dgv.entities.Task;

public interface IManagerService {

	Employee createNewUser(Employee employee) throws Exception;

	Task createNewTask(Task task, Long id) throws Exception;


	List<MetricsDto> getMetricsOfAll(long mgrid);

	List<Employee> getAllEmployees(long mgrid);

	List<Task> getAllTasks(long empId) throws Exception;

	String deleteEmployee(long empId);

	String editEmployee(long empId, Employee emp);

	Employee findEmployeeById(long empId);

	String deleteTask(long taskId);


}
