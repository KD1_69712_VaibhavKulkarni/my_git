package com.dgv.service.impl;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.dgv.entities.Employee;
import com.dgv.entities.Roles;
import com.dgv.repository.IUserRepo;
import com.dgv.service.IUserService;

@Service
@Transactional
public class UserServiceImpl implements IUserService {

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private IUserRepo userRepo;

	@Override
	public Employee authenticateUser(String email, String pwd) throws Exception {
		log.info("Authenticating user: {}", email);
		try {
			Employee user = userRepo.findByEmailAndPassword(email, pwd)
					.orElseThrow(() -> new Exception("WRONG CREDENTIALS"));
			log.info("User authenticated successfully: {}", user.getEmployeeId());
			return user;
		} catch (Exception e) {
			log.error("User authentication failed: {}", e.getMessage());
			throw e;
		}
	}

	@Override
	public String changePassword(String password, Long empid) {
		log.info("Changing password for employee: {}", empid);
		if (userRepo.existsById(empid)) {
			Employee employee = userRepo.findById(empid).get();
			employee.setPassword(password);
			userRepo.save(employee);
			log.info("Password changed successfully for employee: {}", empid);
			return "Password Changed Successfully";
		} else {
			log.error("Employee not found: {}", empid);
			return "Employee Not Found";
		}
	}

	@Override
	public long getAllUsers() {
		log.info("Fetching count of all users");
		return userRepo.count();
	}

	@Override
    public void updateUser(Employee employee) {
        userRepo.save(employee);
    }

	 @Override
	    public Employee getUserById(Long id) {
	        return userRepo.findById(id)
	                .orElseThrow(() -> new RuntimeException("User not found"));
	    }

}
