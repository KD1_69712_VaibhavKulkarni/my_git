package com.dgv.service;



import org.springframework.http.ResponseEntity;

import com.dgv.entities.Employee;

public interface IUserService {

	Employee authenticateUser(String email, String pwd) throws Exception;

	String changePassword(String password, Long empid);

	long getAllUsers();

	void updateUser(Employee user);

	Employee getUserById(Long empid);



}
