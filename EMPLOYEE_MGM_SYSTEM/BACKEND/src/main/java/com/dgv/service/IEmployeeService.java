package com.dgv.service;

import java.util.List;

import com.dgv.entities.Task;

public interface IEmployeeService {

	Double getMetricsOfEmployee(long empId) throws Exception;

	List<Task> getIncompleteTasksOfEmployees(long empId) throws Exception;

	String completeTaskWithTaskId(long taskId);
	
}
