package com.dgv.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dgv.entities.Employee;
import com.dgv.entities.Task;
import com.dgv.repository.IEmployeeRepo;
import com.dgv.repository.ITaskRepo;
import com.dgv.service.IEmployeeService;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class EmployeeServiceImpl implements IEmployeeService {

	@Autowired
	private IEmployeeRepo employeeRepo;

	@Autowired
	private ITaskRepo taskRepo;

	/**
	 * Retrieves the metrics of an employee with the given ID.
	 *
	 * @param empId the ID of the employee
	 * @return the metrics of the employee
	 * @throws Exception if the employee is not found
	 */
	@Override
	public Double getMetricsOfEmployee(long empId) throws Exception {
		log.info("Getting metrics for employee with ID: {}", empId);
		Employee emp = employeeRepo.findById(empId).orElseThrow(() -> new Exception("Employee not found"));
		log.info("Metrics retrieved successfully for employee with ID: {}", empId);
		return emp.getPerformanceMetrics();
	}

	/**
	 * Retrieves the list of incomplete tasks for an employee with the given ID.
	 *
	 * @param empId the ID of the employee
	 * @return the list of incomplete tasks
	 * @throws Exception if the employee is not found
	 */
	@Override
	public List<Task> getIncompleteTasksOfEmployees(long empId) throws Exception {
		log.info("Getting incomplete tasks for employee with ID: {}", empId);
		Employee emp = employeeRepo.findById(empId).orElseThrow(() -> new Exception("Employee not found"));
		List<Task> list = taskRepo.findByEmployeeAndTaskStatus(emp, false);
		log.info("Incomplete tasks fetched successfully for employee with ID: {}", empId);
		return list;
	}

	/**
	 * Completes the task with the given task ID.
	 *
	 * @param taskId the ID of the task
	 * @return a message indicating the status of the task completion
	 */
	@Override
	public String completeTaskWithTaskId(long taskId) {
		log.info("Completing task with ID: {}", taskId);
		if (taskRepo.existsById(taskId)) {
			Task t = taskRepo.findById(taskId).get();
			t.setTaskStatus(true);
			Task task = taskRepo.save(t);
			Employee emp = employeeRepo.findById(task.getEmployee().getEmployeeId()).get();
			emp.setPerformanceMetrics(emp.getPerformanceMetrics() + task.getTaskDifficulty().getRate());
			log.info("Task status changed successfully with ID: {}", taskId);
			return "Task Status Changed Successfully";
		} else {
			log.info("Task status could not be changed with ID: {}", taskId);
			return "Task Status Could Not Change";
		}
	}
}
