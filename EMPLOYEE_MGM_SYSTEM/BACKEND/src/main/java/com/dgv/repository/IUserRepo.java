package com.dgv.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dgv.entities.Employee;

public interface IUserRepo extends JpaRepository<Employee, Long>  {

	Optional<Employee> findByEmailAndPassword(String email, String pwd);

}
