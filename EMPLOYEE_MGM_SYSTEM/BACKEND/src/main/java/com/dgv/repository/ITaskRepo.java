package com.dgv.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dgv.entities.Employee;
import com.dgv.entities.Task;

@Repository
public interface ITaskRepo extends JpaRepository<Task, Long>{
	List<Task> findByEmployee(Employee emp);

	List<Task> findByEmployeeAndTaskStatus(Employee emp, boolean b);
	
//	@Query(value = "select count(employee_id) "
//			+ "from training_database.vaibhav_kulkarni_spring_tasks "
//			+ "where employee_id= :empId and task_status=1;",nativeQuery = true)
//	Long getCountOfCompletedTask(@Param("empId") Long empId);
}
