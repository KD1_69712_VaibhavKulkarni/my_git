package com.dgv.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dgv.entities.Employee;
import com.dgv.entities.Task;

public interface IEmployeeRepo  extends JpaRepository<Employee, Long> {
	List<Employee> findByManagerId(Long id);


	

}
