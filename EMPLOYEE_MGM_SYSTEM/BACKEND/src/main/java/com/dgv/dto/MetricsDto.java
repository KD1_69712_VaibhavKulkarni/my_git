package com.dgv.dto;

public class MetricsDto {
	private Long employeeId;
	private String employeeName;
	private Double performanceMetrics;
	private Long taskCompleted;

	public MetricsDto() {
	}

	public MetricsDto(Long employeeId, String employeeName, Double performanceMetrics, Long taskCompleted) {
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.performanceMetrics = performanceMetrics;
		this.taskCompleted = taskCompleted;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Double getPerformanceMetrics() {
		return performanceMetrics;
	}

	public void setPerformanceMetrics(Double performanceMetrics) {
		this.performanceMetrics = performanceMetrics;
	}

	public Long getTaskCompleted() {
		return taskCompleted;
	}

	public void setTaskCompleted(Long taskCompleted) {
		this.taskCompleted = taskCompleted;
	}

	@Override
	public String toString() {
		return "MetricsDto [employeeId=" + employeeId + ", employeeName=" + employeeName + ", performanceMetrics="
				+ performanceMetrics + ", taskCompleted=" + taskCompleted + "]";
	}

}
