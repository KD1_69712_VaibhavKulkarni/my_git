package com.dgv.dto;

import org.springframework.beans.BeanUtils;

import com.dgv.entities.Rating;
import com.dgv.entities.Task;

public class AssignTaskDTO {
	private Long taskId;
	private String taskName;
	private Boolean taskStatus;
	private String difficulty;
	private Long employeeId;

	
	public AssignTaskDTO() {
	}

	public AssignTaskDTO(String taskName, Boolean taskStatus, String difficulty, Long employeeId) {
		this.taskName = taskName;
		this.taskStatus = taskStatus;
		this.difficulty = difficulty;
		this.employeeId = employeeId;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Boolean getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(Boolean taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	

	@Override
	public String toString() {
		return "AssignTaskDTO [taskName=" + taskName + ", difficulty=" + difficulty + ", taskStatus=" + taskStatus
				+ ", employeeId=" + employeeId + "]";
	}

	public static Task ToEntity(AssignTaskDTO dto) {
		Task task = new Task();
		BeanUtils.copyProperties(dto, task);
		task.setTaskDifficulty(Rating.valueOf(dto.getDifficulty().toUpperCase()));
		return task;
	}
	public static AssignTaskDTO ToDto(Task entity) {
		AssignTaskDTO dto = new AssignTaskDTO();
		BeanUtils.copyProperties(entity, dto);
		dto.setEmployeeId(entity.getEmployee().getEmployeeId());
		
		return dto;
	}

}
