package com.dgv.dto;

import org.springframework.beans.BeanUtils;

import com.dgv.entities.Employee;
import com.dgv.entities.Roles;

public class EmployeeDTO {
	private String employeeName;
	private String email;
	private String password;
	private Double performanceMetrics;
	private Roles role;
	private Long managerId;

	public EmployeeDTO() {
	}

	public EmployeeDTO(String employeeName, String email, String password,Long managerId) {
		this.employeeName = employeeName;
		this.email = email;
		this.password = password;
		this.managerId = managerId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getPerformanceMetrics() {
		return performanceMetrics;
	}

	public void setPerformanceMetrics(Double performanceMetrics) {
		this.performanceMetrics = performanceMetrics;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	@Override
	public String toString() {
		return "EmployeeDTO [employeeName=" + employeeName + ", email=" + email + ", performanceMetrics="
				+ performanceMetrics + ", role=" + role + ", managerId=" + managerId + "]";
	}

	public static Employee ToEntity(EmployeeDTO dto) {
		Employee entity = new Employee();
		BeanUtils.copyProperties(dto, entity);
		return entity;
	}

	public static EmployeeDTO fromEntity(Employee entity) {
		EmployeeDTO dto = new EmployeeDTO();
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

}
