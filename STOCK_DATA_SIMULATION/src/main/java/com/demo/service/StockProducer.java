package com.demo.service;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class StockProducer {

    private static final String TOPIC = "stock_data";
    @Autowired
    private  KafkaTemplate<String, String> kafkaTemplate;
    private final Random random = new Random();


    @Scheduled(fixedRate = 500)
    public void produceStock() throws JsonProcessingException {
        double price = 900.00;
		System.out.println("HI IN PRODUCER");

		// Randomly increase or decrease the stock price
		if ((random.nextInt() * 10) % 2 == 0 && price >= 0) {
		    price = reduceStock(price);
		} else {
		    price = increaseStock(price);
		}

		// Send the updated price to Kafka
		kafkaTemplate.send(TOPIC, String.valueOf(String.format("%.2f", price)));
		System.out.println("Produced stock price: " + String.format("%.2f", price));
    }

    private double reduceStock(double price) {
        // Logic to reduce stock price
        double reducedPrice = price - (random.nextDouble() * 10);
        System.out.println("Reduced stock price to: " + reducedPrice);
        return reducedPrice;
    }

    private double increaseStock(double price) {
        // Logic to increase stock price
        double increasedPrice = price + (random.nextDouble() * 10);
        System.out.println("Increased stock price to: " + increasedPrice);
        return increasedPrice;
    }
}
