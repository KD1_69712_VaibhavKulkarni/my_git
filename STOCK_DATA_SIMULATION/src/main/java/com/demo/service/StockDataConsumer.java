package com.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class StockDataConsumer {
	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	
	 @KafkaListener(topics = "stock_data", groupId = "group_id")
	    public void consume(String message) {
		 System.out.println("HI IN CONSUMER");
	        messagingTemplate.convertAndSend("/topic/stock_data", message);
	    }
}
